### How to set the program up ###

1. Download Python (v2.7).
2. Make sure the 'nltk' package has been installed.
3. Download the source.
4. Create a folder 'nltk_resources' inside 'peoplescience/peoplescience/model'.
5. From 'http://nlp.stanford.edu/software/CRF-NER.shtml#Download', download the .zip file and extract its contents to 'peoplescience/peoplescience/model/nltk_resources/ner/'.
6. From 'http://nlp.stanford.edu/software/tagger.shtml#Download', download the .zip file and extract its contents to 'peoplescience/peoplescience/model/nltk_resources/postagger/'.
7. Copy the file 'example_ps_config' (located at the root of the files), rename it to 'ps_config' and fill out connection credentials and information.
8. Run 'run_server.py' (located at the root of the files).
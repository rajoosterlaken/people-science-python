from flask import jsonify, json, request, make_response
from peoplescience import app


# Default responses.

def no_post():
		return make_response(jsonify({
			"status": "error",
			"message": "Request method was not POST."
			}), 400)

def not_parsable():
	return make_response(jsonify({
		"status": "error",
		"message": "JSON structure invalid."
		}), 400)

def non_existent_key():
	return make_response(jsonify({
		"status": "error",
		"message": "Identification key was not provided."
		}), 400)

def invalid_key(key):
	return make_response(jsonify({
		"status": "error",
		"message": "The provided identification key was invalid."
		}), 400)

def missing_required_keys(missing_keys):
	message = "Missing key(s):"
	for index, key in enumerate(missing_keys):
		message += (" " + key)
		if index < len(missing_keys) - 1:
			message += ","
		else:
			message += "."
	return make_response(jsonify({
		"status": "error",
		"message": message
		}), 400)

def conflict(message):
	return make_response(jsonify({
		"status": "error",
		"message": message
		}), 409)

def default_json(loaded_json):
	return {
		"version": app.version,
		"status": "okay",
		"message": "",
		"data": loaded_json,
		"results": {}
	}

def db_error(error):
	# TODO: ADD WRITING ERROR TO LOG
	return make_response(jsonify({
		"status": "error",
		"message": "A Database Error has occurred."
		}), 500)

def parse(response_json, error_code):
	return make_response(jsonify(response_json), error_code)

# Validation

def validate(request, required_keys):
	if request.method == "POST":
		json = request.get_json(silent=True)
		if json:
			if "dutchKey" in json:
				if json["dutchKey"] == app.key:
					if all([x in json for x in required_keys]):
						return default_json(json)
					else:
						missing = [x for x in required_keys if x not in json]
						return missing_required_keys(missing)
				else:
					return invalid_key(json["dutchKey"])
			else:
				return non_existent_key()
		else:
			return not_parsable()
	else:
		return no_post()

# Logging
@app.before_request
def before_request():
	if request.method == "POST":
		json = request.get_json(silent=True)
		print "New request arrived:"
		print json
from peoplescience import app
from sqlalchemy.exc import DatabaseError
from sqlalchemy.inspection import inspect
from peoplescience.db.objects import *
from peoplescience.db.database import db_session
from peoplescience.model.analyst import queue_answer_analysis
from peoplescience.model import utils
import threading, types, nltk, os
from flask import request
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.tag.stanford import StanfordPOSTagger, StanfordNERTagger
from Queue import Queue

@app.route("/answer/get_processing_status", methods=["GET", "POST"])
def get_processing_status():
	global processing_status
	x = utils.validate(request, ["answer_id"])
	if type(x) == types.DictType:
		answer_id = x["data"]["answer_id"]

		answer = db_session.query(Answer) \
			.filter(Answer.id == answer_id).first()

		if answer:
			if answer.id in processing_status:
				x["results"]["status"] = processing_status[answer.id]
				return utils.parse(x, 200)
			else:
				x["status"] = "error"
				x["message"] = "Answer is not queued for processing."
				return utils.parse(x, 409)
		else:
			return utils.conflict("Non-existent answer id: '%s'." % (answer_id))
	else:
		return x

def check_tagging_status(answer_id):
	global processing_status
	if answer_id in processing_status:
		return processing_status[answer_id]
	else:
		return "non-queued"

@app.route("/answer/queuetagging", methods=["GET", "POST"])
def remotely_queue_answer_tagging():
	x = utils.validate(request, ["answers"])
	if type(x) == types.DictType:
		answers = x["data"]["answers"]

		for answer_id in answers:
			db_session()
			answer = db_session.query(Answer).filter(Answer.id == answer_id).first()
			if answer:
				queue_answer(answer.id)
				db_session.remove()
			else:
				db_session.remove()
				return utils.conflict("Non-existent answer id: '%s'." % (answer_id))

		return utils.parse(x, 200)
	else:
		return x

def init_tagger():
	global pos_tagger, ner_tagger, chunk_parser
	ner_tagger = StanfordNERTagger("peoplescience/model/nltk_resources/ner/classifiers/english.all.3class.distsim.crf.ser.gz", "peoplescience/model/nltk_resources/ner/stanford-ner.jar")
	pos_tagger = StanfordPOSTagger("peoplescience/model/nltk_resources/postagger/models/english-bidirectional-distsim.tagger", "peoplescience/model/nltk_resources/postagger/stanford-postagger.jar")
	grammar = r"""
		N: {<NN(\w)*>+}
		V: {<V.*>+}
		"""
	chunk_parser = nltk.RegexpParser(grammar)

	global queue, looping_thread, looping, processing_status
	queue = Queue()
	looping = True
	looping_thread = threading.Thread(name="tagger", target=tagger_loop)
	processing_status = {}

	setup_windows_hook()

	looping_thread.start()
	print "Tagger initialized."

def setup_windows_hook():
	if os.name == "nt":
		try:
			import win32api, thread
		except ImportError, e:
			print "Make sure to install the 'win32api' Python module first!"
		else:
			def handler(sig, hook=thread.interrupt_main):
				hook()
				return 1
			win32api.SetConsoleCtrlHandler(handler, 1)

def shutdown_tagger():
	global queue, looping_thread, looping
	looping = False
	queue.put(None)
	looping_thread.join()

def queue_answer(answer_id):
	global queue, processing_status
	queue.put(answer_id)
	processing_status[answer_id] = "queued"

def tagger_loop():
	global looping, queue, processing
	while looping:
		answer_id = queue.get()
		if answer_id != None:
			processing_status[answer_id] = "processing"
			db_session()
			try:
				answer = db_session.query(Answer).filter(Answer.id == answer_id).first()
				if answer != None:
					tag(answer)
			except Exception as e:
				print "[Tagger] ", str(e)
			db_session.close()
			processing_status[answer_id] = "done"
		queue.task_done()
	print "Tagger finished."

def tag(answer):
	global pos_tagger, ner_tagger

	result = []
	sent_tokens = sent_tokenize(answer.text)
	word_tokens = [word_tokenize(sent) for sent in sent_tokens]

	if word_tokens:
		pos_tags = []
		try:
			pos_tags = pos_tagger.tag([x for y in word_tokens for x in y])
		except UnicodeEncodeError as e:
			print "[Tagger] An UnicodeEncodeError occurred:\n", str(e)
		chunked = chunk(pos_tags)
		result += chunked

		ner_tags = []
		try:
			ner_tags = ner_tagger.tag_sents(word_tokens)
		except Exception as e:
			print "[Tagger] An unexpected error occurred:\n", str(e)
		entities = group_entities([tag for tagset in ner_tags for tag in tagset])
		result += entities

	save_to_database(answer, result)
	answer_id = answer.id
	db_session.expunge(answer)
	queue_answer_analysis(answer_id)

def save_to_database(answer, tags):
	counted_tags = count(tags)
	for key in counted_tags:
		word, pos_tag = key
		occurrences = counted_tags[key]
		tag = Tag(answer.id, word, pos_tag, occurrences)
		db_session.add(answer)
		answer.tags.append(tag)
	try:
		db_session.commit()
	except DatabaseError:
		# TODO: Add error to log for all DatabaseErrors.
		pass

def count(tags):
	counted_tags = {}
	for tag in tags:
		if type(tag) == types.TupleType:
			if len(tag) > 2:
				print "Strange tuple found:", tag

			word, pos_tag = tag[0], tag[1]
			if (word.lower(), pos_tag) in counted_tags:
				counted_tags[(word.lower(), pos_tag)] += 1
			else:
				counted_tags[(word.lower(), pos_tag)] = 1
	return counted_tags

def chunk(pos_tags):
	global chunk_parser
	chunked = chunk_parser.parse(pos_tags)
	result =  []
	for chunk in chunked:
		if type(chunk[0]) == types.TupleType:
			if chunk.label() in ["N", "V"]:
				result += [l for l in chunk.leaves()]
	return result

def group_entities(ner_tags):
	entity_index = -1
	entities = []
	last_type = None
	for word, tag_type in ner_tags:
		if tag_type in [u"LOCATION", u"ORGANIZATION", u"PERSON"]:
			if last_type == tag_type: 
				entities[entity_index] += " " + word
			else:
				entity_index += 1
				last_type = tag_type
				entities.append("#" + word)
		elif tag_type != u"O":
			print "Undefined tag type found:", tag_type
	return entities

@app.teardown_appcontext
def shutdown_session(exception=None):
	db_session.remove()

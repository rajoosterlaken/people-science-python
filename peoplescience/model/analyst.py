from peoplescience import app
import threading
from Queue import Queue
from peoplescience.db.objects import *
from peoplescience.db.database import db_session
from nltk.corpus import wordnet as wn
from peoplescience.model import utils
from flask import request
import types
from sqlalchemy.exc import DatabaseError

def init_analyst():
	global queue, looping_thread, looping, analysis_status
	queue = Queue()
	looping = True
	looping_thread = threading.Thread(name="analyst", target=analyst_loop)
	analysis_status = {}

	looping_thread.start()
	print "Analyst initialized."

@app.route("/answer/get_analysis_status", methods=["GET", "POST"])
def get_analysis_status():
	global analysis_status
	x = utils.validate(request, ["answer_id"])
	if type(x) == types.DictType:
		answer_id = x["data"]["answer_id"]

		answer = db_session.query(Answer).filter(Answer.id == answer_id).first()

		print "analysis_status:", analysis_status

		if answer:
			if answer.id in analysis_status:
				x["results"]["status"] = analysis_status[answer.id]
				return utils.parse(x, 200)
			else:
				x["status"] = "error"
				x["message"] = "Answer is not queued for processing."
				return utils.parse(x, 409)
		else:
			return utils.conflict("Non-existent answer id: '%s'." % (answer_id))

		return utils.parse(x, 200)
	else:
		return x

def check_analysis_status(answer_id):
	global analysis_status
	if answer_id in analysis_status:
		return analysis_status[answer_id]
	else:
		return "non-queued"

def shutdown_analyst():
	global queue, looping_thread, looping
	looping = False
	queue.put(None)
	looping_thread.join()

def queue_answer_analysis(answer_id):
	global queue, analysis_status
	queue.put(answer_id)
	analysis_status[answer_id] = "queued"

@app.route("/answer/queueanalysis", methods=["GET", "POST"])
def remotely_queue_answer_analysis():
	x = utils.validate(request, ["answers"])
	if type(x) == types.DictType:
		answers = x["data"]["answers"]

		for answer_id in answers:
			db_session()
			answer = db_session.query(Answer).filter(Answer.id == answer_id).first()
			if answer:
				queue_answer_analysis(answer.id)
				db_session.remove()
			else:
				db_session.remove()
				return utils.conflict("Non-existent answer id: '%s'." % (answer_id))

		return utils.parse(x, 200)
	else:
		return x

def analyst_loop():
	global queue, looping, analysis_status
	while looping:
		answer_id = queue.get()
		analysis_status[answer_id] = "processing"
		if answer_id != None:
			db_session()
			answer = db_session.query(Answer).filter(Answer.id == answer_id).first()
			if answer != None:
				analyse(answer)
				answer.processing_status = "ANALYSIS_COMPLETED"
				db_session.add(answer)
				db_session.commit()
			db_session.remove()
		analysis_status[answer_id] = "done"
		queue.task_done()
	print "Analyst finished."

def analyse(answer):
	question = answer.question
	if question != None:
		other_answers = [a for a in question.answers if a.id != answer.id]
		for other_answer in other_answers:
			# TODO: Listen for last disabled tags and disable those, perhaps.
			# TODO: However, think about whether or not this influences the automatic calling of calc_answer_sim in the clusterer tree.
			similarity = calculate_answer_similarity(answer, other_answer, [])
			save_answer_similarity(answer.id, other_answer.id, similarity)

def calculate_answer_similarity(answer1, answer2, t_weights):
	def average(values):
		if len(values) <= 0:
			return 0.0

		value_sum = 0.0
		for value in values:
			value_sum += value
		return value_sum / len(values)

	similarities = []
	for tag1 in answer1.tags:
		for tag2 in answer2.tags:
			word1, pos1, word2, pos2 = (tag1.text, tag1.pos, tag2.text, tag2.pos) \
				if tag1.text < tag2.text \
				else (tag2.text, tag2.pos, tag1.text, tag1.pos)

			toBeContinued = False
			for t_weight in t_weights:
				if "text" in t_weight and "importance" in t_weight:
					if (word1 == t_weight["text"] or word2 == t_weight["text"]) and not t_weight["importance"]:
						toBeContinued = True

			if toBeContinued:
				similarities.append(0.0)
				continue

			value = None
			if word1 == word2 and pos1 == pos2:
				value = 1
				similarities.append(1.0)
				continue

			value = check_for_existent_word_similarity(word1, pos1, word2, pos2)
			if value == None:
				value = calculate_word_similarity(word1, pos1, word2, pos2)
			similarities.append(value)

	return average(similarities)

def calculate_word_similarity(word1, pos1, word2, pos2):
	def highest_sim(synset1, synset2):
		highest_match = 0.0
		for syn1 in synset1:
			for syn2 in synset2:
				newest_match = syn1.path_similarity(syn2)
				if newest_match > highest_match:
					highest_match = newest_match
		return highest_match

	def average_sim(synset1, synset2):
		total_match = 0.0
		count = 0
		for syn1 in synset1:
			for syn2 in synset2:
				similarity = syn1.path_similarity(syn2)
				total_match += (similarity if similarity != None else 0.0)
				count += 1
		return total_match / count if count > 0 else 0.0

	def get_synsets(word):
		synsets = wn.synsets(word)
		nouns = [x for x in synsets if x.pos() == u'n']
		verbs = [x for x in synsets if x.pos() == u'v']
		return nouns, verbs

	w1_nouns, w1_verbs = get_synsets(word1)
	w2_nouns, w2_verbs = get_synsets(word2)

	if (w1_nouns == None and w1_verbs == None) \
		or (w2_nouns == None and w2_verbs == None):
		save_word_similarity(word1, pos1, word2, pos2, 0.0)
		return 0.0

	value = None
	if pos1.startswith("N") and pos2.startswith("N"):
		if w1_nouns != None and w2_nouns != None:
			value = highest_sim(w1_nouns, w2_nouns)
	elif pos1.startswith("N") and pos2.startswith("N"):
		value = highest_sim(w1_verbs, w2_verbs)

	if value == None:
		value = average_sim(w1_nouns + w1_verbs, w2_nouns + w2_verbs)

	save_word_similarity(word1, pos1, word2, pos2, value)
	return value

def check_for_existent_word_similarity(word1, pos1, word2, pos2):
	wordsim = db_session.query(WordSimilarity).filter(
		WordSimilarity.word1 == word1,
		WordSimilarity.pos1 == pos1,
		WordSimilarity.word2 == word2,
		WordSimilarity.pos2 == pos2
		).first()
	return wordsim.score if wordsim != None else None

def save_word_similarity(word1, pos1, word2, pos2, score):
	wordsim = WordSimilarity(word1, pos1, word2, pos2, score)
	db_session.add(wordsim)
	try:
		db_session.commit()
	except DatabaseError:
		pass

def save_answer_similarity(a_id1, a_id2, score):
	a1, a2 = (a_id1, a_id2) if a_id1 < a_id2 else (a_id2, a_id1)
	answersim = db_session.query(AnswerSimilarity).filter(
		AnswerSimilarity.answer1_id == a1,
		AnswerSimilarity.answer2_id == a2
		).first()

	if answersim != None:
		answersim.score = score
	else:
		answersim = AnswerSimilarity(a1, a2, score)
	try:
		db_session.add(answersim)
		db_session.commit()
		db_session.expunge(answersim)
	except DatabaseError:
		pass

@app.teardown_appcontext
def shutdown_session(exception=None):
	db_session.remove()
from peoplescience import app
from peoplescience.db.database import db_session

@app.route("/")
def index():
	return (
		"<h1>PeopleScience %s</h1>"
		"<h2>Database</h2>"
		"<b>Running</b>: %s"
		) % (app.version, db_session.is_active)

@app.teardown_appcontext
def shutdown_session(exception=None):
	db_session.remove()
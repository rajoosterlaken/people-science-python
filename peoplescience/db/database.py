from peoplescience import app
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

con_str = (
	"mysql+mysqlconnector://%(user)s:%(password)s@"
	"%(host)s:%(port)s/%(schema)s"
	) % {
	"user":     app.db_user,
	"password": app.db_password,
	"host":     app.db_host,
	"port":     app.db_port,
	"schema":   app.db_schema
	}

# TODO: Catch error on using invalid connection.
engine = create_engine(con_str, pool_recycle=50)

db_session = scoped_session(sessionmaker(
	autocommit=False,
	autoflush=False,
	bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
	import peoplescience.db.objects
	Base.metadata.create_all(bind=engine)
	print "Database initialized."
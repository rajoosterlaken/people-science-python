from sqlalchemy import Table, Column, Integer, BigInteger, String, Text, Float, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from peoplescience.db.database import Base

class Question(Base):
	__tablename__ = "question"
	id = Column(BigInteger, primary_key=True)
	text = Column(String(255))
	type = Column(String(255))
	answers = relationship("Answer", backref="question")

	def __init__(self, text, creator_id):
		self.text = text
		self.creator_id = creator_id

	def __repr__(self):
		rest = "system-made"
		if self.creator_id != None:
			rest = "creator_id='%s'" % (self.creator_id)
		return "<Question %s, %s>" % (self.id, rest)

class Answer(Base):
	__tablename__ = "answer"
	id = Column(BigInteger, primary_key=True)
	text = Column(Text)
	question_id = Column(BigInteger, ForeignKey("question.id"))
	tags = relationship("Tag", backref="answer")
	processing_status = Column(String(255))

	def __init__(self, question_id, participant_id, text):
		self.question_id = question_id
		self.participant_id = participant_id
		self.text = text

	def __repr__(self):
		return "<Answer %s, participant_id='%s'>" % (self.id, self.participant_id)

class Tag(Base):
	__tablename__ = "tag"
	id = Column(BigInteger, primary_key=True)
	text = Column(String(255))
	pos = Column(String(255))
	occurrences = Column(Integer)
	answer_id = Column(BigInteger, ForeignKey("answer.id"))

	def __init__(self, answer_id, text, pos, occurrences):
		self.answer_id = answer_id
		self.text = text
		self.pos = pos
		self.occurrences = occurrences

	def __repr__(self):
		return "<Tag %s>" % (self.id)

class WordSimilarity(Base):
	__tablename__ = "word_similarities"
	id = Column(BigInteger, primary_key=True)
	word1 = Column(String(255))
	pos1 = Column(String(255))
	word2 = Column(String(255))
	pos2 = Column(String(255))
	score = Column(Float)

	def __init__(self, w1, p1, w2, p2, score):
		self.word1, self.pos1, self.word2, self.pos2 = (w1, p1, w2, p2) if w1 < w2 else (w2, p2, w1, p1)
		self.score = score

	def __repr__(self):
		return "<WordSimilarity(%s,%s), score='%s'>" % (self.word1, self.word2, self.score)

class AnswerSimilarity(Base):
	__tablename__ = "answer_similarities"
	id = Column(BigInteger, primary_key=True)
	answer1_id = Column(BigInteger, ForeignKey("answer.id"))
	answer2_id = Column(BigInteger, ForeignKey("answer.id"))
	score = Column(Float)

	def __init__(self, a_id1, a_id2, score):
		self.answer1_id, self.answer2_id = (a_id1, a_id2) if a_id1 < a_id2 else (a_id2, a_id1)
		self.score = score

	def __repr__(self):
		return "<AnswerSimilarity(%s,%s), score='%s'>" % (self.answer1_id, self.answer2_id, self.score)
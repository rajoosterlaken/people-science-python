from flask import Flask

app = Flask("PeopleScience")
app.version = "v1.0"

try:
	ps_config = file("ps_config", "r")
except IOError:
	print "No 'ps_config' file found in root."
	import sys
	sys.exit(0)
else:
	for l in ps_config:
		if not (l.startswith("#") or len(l) == 1):
			key, val = tuple(l.strip().split("=", 1))
			if key == "key":
				app.key = val
			elif key == "api_host":
				app.api_host = val
			elif key == "api_port":
				app.api_port = int(val)
			elif key == "db_host":
				app.db_host = val
			elif key == "db_port":
				app.db_port = int(val)
			elif key == "db_user":
				app.db_user = val
			elif key == "db_password":
				app.db_password = val
			elif key == "db_schema":
				app.db_schema = val
	ps_config.close()

from peoplescience.db.database import init_db
init_db()

from peoplescience.model.tagger import init_tagger
init_tagger()

from peoplescience.model.analyst import init_analyst
init_analyst()

import peoplescience.home
import peoplescience.model
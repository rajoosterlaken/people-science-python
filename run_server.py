if __name__ == '__main__':
	from peoplescience import app
	from peoplescience.model.tagger import shutdown_tagger
	from peoplescience.model.analyst import shutdown_analyst
	app.run(host=app.api_host, port=app.api_port, debug=True, use_reloader=False, threaded=True)
	shutdown_tagger()
	shutdown_analyst()